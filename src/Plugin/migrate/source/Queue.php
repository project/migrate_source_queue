<?php

namespace Drupal\migrate_source_queue\Plugin\migrate\source;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\DelayableQueueInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\migrate\Plugin\migrate\source\SourcePluginBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\migrate_source_queue\QueueIterator;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @MigrateSource(
 *   id = "queue",
 * )
 */
class Queue extends SourcePluginBase implements ContainerFactoryPluginInterface {

  protected QueueFactory $queueFactory;
  protected QueueWorkerManagerInterface $queueManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    $instance = new static($configuration, $plugin_id, $plugin_definition, $migration);
    $instance->queueFactory = $container->get('queue');
    $instance->queueManager = $container->get('plugin.manager.queue_worker');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'data' => $this->t('The same as what passed into createItem().'),
      'item_id' => $this->t('The unique ID returned from createItem().'),
      'created' => $this->t('Timestamp when the item was put into the queue.'),
    ];

    if (isset($this->configuration['fields']) && is_array($this->configuration['fields'])) {
      if ($this->isArrayAssociative($this->configuration['fields'])) {
        $fields += $this->configuration['fields'];
      }
      else {
        $fields += array_flip($this->configuration['fields']);
      }
    }

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $fields = $this->configuration['fields'] ?? [];
    $fieldNames = $this->isArrayAssociative($fields)
      ? array_keys($fields)
      : array_values($fields);

    // Assign the fields from the data array to the row.
    $data = $row->getSourceProperty('data');
    foreach ($fieldNames as $field) {
      if (array_key_exists($field, $data)) {
        $row->setSourceProperty($field, $data[$field]);
      }
    }

    // Temporarily unset all non-fields from the source, so they are not included in the hash.
    $nonFieldNames = array_diff(array_keys($row->getSource()), $fieldNames);
    $nonFieldValues = [];

    foreach ($nonFieldNames as $nonFieldName) {
      $nonFieldValues[$nonFieldName] = $row->getSourceProperty($nonFieldName);
      $row->setSourceProperty($nonFieldName, NULL);
    }

    // Determine if the row needs processing.
    $processRow = parent::prepareRow($row);

    // If processing is not necessary, delete the item from the queue.
    if ($processRow === FALSE || !$this->needsProcessing($row)) {
      $queue = $this->queueFactory->get($row->get('queue_name'));
      $item = (object) ['item_id' => $row->get('item_id')];
      $queue->deleteItem($item);
    }

    // Restore the non-field values to the source.
    foreach ($nonFieldNames as $nonDataKey) {
      $row->setSourceProperty($nonDataKey, $nonFieldValues[$nonDataKey]);
    }

    return $processRow;
  }

  /**
   * {@inheritdoc}
   */
  public function __toString() {
    return $this->configuration['queue_name'];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds(): array {
    return $this->configuration['keys'];
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    return new QueueIterator(
      $this->configuration['queue_name'],
      array_keys($this->configuration['keys']),
    );
  }

  /**
   * Determines whether a row needs processing.
   */
  protected function needsProcessing(Row $row): bool {
    return !$row->getIdMap() || $row->needsUpdate() || $this->aboveHighWater($row) || $this->rowChanged($row);
  }

  /**
   * Determines whether an array is associative or sequential.
   */
  protected function isArrayAssociative(array $value): bool {
    return array_keys($value) !== array_keys(array_keys($value));
  }

}
