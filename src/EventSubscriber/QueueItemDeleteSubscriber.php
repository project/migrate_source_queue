<?php

namespace Drupal\migrate_source_queue\EventSubscriber;

use Drupal\Core\Queue\QueueFactory;
use Drupal\migrate\Event\MigrateEvents;
use Drupal\migrate\Event\MigratePostRowSaveEvent;
use Drupal\migrate_source_queue\Plugin\migrate\source\Queue;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Deletes processed queue items.
 */
class QueueItemDeleteSubscriber implements EventSubscriberInterface {

  protected QueueFactory $queueFactory;

  /**
   * Constructs a QueueItemDeleteSubscriber instance.
   *
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   */
  public function __construct(
    QueueFactory $queue_factory
  ) {
    $this->queueFactory = $queue_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[MigrateEvents::POST_ROW_SAVE] = ['onPostRowSave'];

    return $events;
  }

  public function onPostRowSave(MigratePostRowSaveEvent $event): void {
    if (!$event->getMigration()->getSourcePlugin() instanceof Queue) {
      return;
    }

    // Queue item is processed, release it.
    $row = $event->getRow();
    $queue = $this->queueFactory->get($row->get('queue_name'));
    $item = (object) ['item_id' => $row->get('item_id')];

    $queue->deleteItem($item);
  }

}
