<?php

namespace Drupal\migrate_source_queue;

use Drupal\Core\Queue\QueueGarbageCollectionInterface;
use Drupal\Core\Queue\QueueInterface;

/**
 * Iterates over a queue, automatically claiming and releasing queue items.
 */
class QueueIterator implements \Iterator, \Countable {

  protected ?QueueInterface $queue;
  protected ?\stdClass $currentItem = NULL;

  protected string $queueName;
  protected array $sourceIdKeys;

  /**
   * Constructs a QueueIterator instance.
   *
   * @param $queue_name
   *   The name of the queue that will be processed.
   * @param $source_id_keys
   *   The source ID's that will be added to the queue item data in order to
   *   pass validation in @see \Drupal\migrate\Row::__construct
   */
  public function __construct(string $queue_name, array $source_id_keys) {
    $this->queueName = $queue_name;
    $this->sourceIdKeys = $source_id_keys;
  }

  /**
   * {@inheritdoc}
   */
  #[\ReturnTypeWillChange]
  public function count() {
    return $this->getQueue()->numberOfItems();
  }

  /**
   * {@inheritdoc}
   */
  #[\ReturnTypeWillChange]
  public function rewind() {
    if (isset($this->queue)) {
      if (isset($this->currentItem)) {
        $this->queue->releaseItem($this->currentItem);
        $this->currentItem = NULL;
      }

      $this->queue = NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  #[\ReturnTypeWillChange]
  public function valid() {
    if (!isset($this->currentItem)) {
      $currentItem = $this->getQueue()->claimItem();

      if ($currentItem === FALSE) {
        $this->currentItem = NULL;
      }
      else {
        $this->currentItem = $currentItem;
        /* Add source ID's here to pass validation in @see \Drupal\migrate\Row::__construct */
        foreach ($this->sourceIdKeys as $sourceIdKey) {
          $this->currentItem->$sourceIdKey = $this->currentItem->data[$sourceIdKey] ?? NULL;
        }
      }
    }

    return $this->currentItem !== NULL;
  }

  /**
   * {@inheritdoc}
   */
  #[\ReturnTypeWillChange]
  public function current() {
    if ($this->currentItem instanceof \stdClass) {
      return (array) $this->currentItem;
    }

    return $this->currentItem;
  }

  /**
   * {@inheritdoc}
   */
  #[\ReturnTypeWillChange]
  public function key() {
    if ($this->currentItem === NULL) {
      return NULL;
    }

    return $this->currentItem->item_id;
  }

  /**
   * {@inheritdoc}
   */
  #[\ReturnTypeWillChange]
  public function next() {
    if (isset($this->currentItem)) {
      $this->currentItem = NULL;
    }
  }

  /**
   * Create the queue instance.
   */
  protected function getQueue(): QueueInterface {
    if (isset($this->queue)) {
      return $this->queue;
    }

    // Make sure every queue exists. There is no harm in trying to recreate
    // an existing queue.
    $this->queue = \Drupal::queue($this->queueName);
    $this->queue->createQueue();

    // Run garbage collection on the queue so expired items will be released.
    if ($this->queue instanceof QueueGarbageCollectionInterface) {
      $this->queue->garbageCollection();
    }

    return $this->queue;
  }

}
